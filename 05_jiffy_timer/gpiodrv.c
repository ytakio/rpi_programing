#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/errno.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/stat.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <asm/uaccess.h>

#include "rpi2_gpio_reg.h"
#include "rpi_gpio.h"

MODULE_AUTHOR("Takio Yamaoka");
MODULE_LICENSE("GPL");

#define NUM_OF_DEVS		1
#define DEVNAME			"gpiodrv"
#define MAJOR_NUMBER	0	// as auto numbering
#define MINOR_NUMBER	0

static int _dev_major = MAJOR_NUMBER;
static int _dev_minor = MINOR_NUMBER;

/* pointer to cdev array */
static struct cdev *gp_cdev_array = NULL;

/* pointer to device class */
static struct class *gp_class = NULL;

/* name for iommap */
#define MAPNAME_FOR_GPIO	"gpiodrv_map"

/* pointer to GPIO register */
static void __iomem *gp_gpio_map = NULL;

/* Chennel of GPIO */
#define GPIO_CH		7

/* status of this device */
static int gi_gpio_level = 0;

static int gpiodrv_map(void)
{
	// request to resetve region of memory
	if (request_mem_region(RPI_GPIO_BASE, RPI_BLOCK_SIZE, MAPNAME_FOR_GPIO) != 0) {
		printk(KERN_ALERT "request_mem_region failed\n");
		return -EBUSY;
	}
	// execute mapping
	gp_gpio_map = ioremap_nocache(RPI_GPIO_BASE, RPI_BLOCK_SIZE);
	rpi_gpio_init((uint32_t*)gp_gpio_map);
	return 0;
}

static void gpiodrv_unmap(void)
{
	// execute unmapping
	rpi_gpio_release();
	iounmap(gp_gpio_map);
	gp_gpio_map = NULL;
	// release requested region of memory
#if 0	// I don't why it got warning "try to free nonexistent"
	release_mem_region(RPI_GPIO_BASE, RPI_BLOCK_SIZE);
#endif
}

static int gpiodrv_open(struct inode *inode, struct file *filep)
{
	int retval;

	if (gp_gpio_map != NULL) {
		printk(KERN_ERR "gpiodrv is already opened.\n");
		return -EBUSY;
	}
	// execute mapping
	retval = gpiodrv_map();
	if (retval != 0) {
		printk(KERN_ERR "Can not open gpiodrv.\n");
		return retval;
	}
	// success
	return 0;
}

static int gpiodrv_release(struct inode *inode, struct file *filep)
{
	gpiodrv_unmap();
	return 0;
}

static ssize_t gpiodrv_write(struct file *filep, const char __user *buf, size_t count, loff_t *f_pos)
{
	char cvalue;

	if (0 < count) {
		if (copy_from_user(&cvalue, buf, sizeof(char)) != 0) {
			printk(KERN_ALERT "copy_from_user failed.\n");
			return -EFAULT;
		}
		if ((cvalue & 0x1) == 0) {
			gi_gpio_level = 0;
			rpi_gpio_clearpin(GPIO_CH);
		}
		else {
			gi_gpio_level = 1;
			rpi_gpio_setpin(GPIO_CH);
		}
		printk(KERN_INFO "gpiodrv_write:%d\n", gi_gpio_level);
		return sizeof(char);
	}
	return 0;
}

struct file_operations gp_gpiodrv_fops = {
	.open		= gpiodrv_open,
	.release	= gpiodrv_release,
	.write		= gpiodrv_write,
};

static int gpiodrv_register_dev(void)
{
	int retval;
	dev_t dev;
	size_t size;
	int i;

	/* alloc character device */
	retval = alloc_chrdev_region(
			&dev,
			MINOR_NUMBER,
			NUM_OF_DEVS,
			DEVNAME);
	if (retval < 0) {
		printk(KERN_ERR "alloc_chrdev_region failed.\n");
		return retval;
	}
	
	// purse major number
	_dev_major = MAJOR(dev);

	/* Create device class */
	gp_class = class_create(THIS_MODULE, DEVNAME);
	if (IS_ERR(gp_class)) {
		return PTR_ERR(gp_class);
	}

	/* alloc cdev */
	size = sizeof(struct cdev) * NUM_OF_DEVS;
	gp_cdev_array = (struct cdev*)kmalloc(size, GFP_KERNEL);

	/* register character devices */
	for ( i = 0; i < NUM_OF_DEVS; i++) {
		dev_t devno = MKDEV(_dev_major, _dev_minor + i);
		cdev_init(&gp_cdev_array[i], &gp_gpiodrv_fops);
		gp_cdev_array[i].owner = THIS_MODULE;
		// register
		if (cdev_add(&gp_cdev_array[i], devno, 1) < 0) {
			// failed register
			printk(KERN_ERR "cdev_add failed minor:%d\n", MINOR(devno));
		}
		else {
			// create node
			device_create(
					gp_class,
					NULL,
					devno,
					NULL,	// parameter ?
					DEVNAME"%u", MINOR(devno));
		}
	}
	return 0;
}

static void gpiodrv_gpio_setup(void)
{
	gi_gpio_level = 1;
	rpi_gpio_setpin(GPIO_CH);
}

static void gpiodrv_gpio_endroll(void)
{
	rpi_gpio_getpin(GPIO_CH);
}

static void timer_handler(unsigned long data);
static struct timer_list jiffy_timer;
#define JIFFY_DIFF (500*HZ/1000)
static void register_jiffy_timer(void)
{
	init_timer(&jiffy_timer);
	jiffy_timer.data = jiffies;
	jiffy_timer.expires = jiffies + JIFFY_DIFF;
	jiffy_timer.function = timer_handler;
	// register
	add_timer(&jiffy_timer);
}

static void timer_handler(unsigned long data)
{
	if (gpiodrv_map() == 0) {
		gi_gpio_level ^= 1;
		if (gi_gpio_level == 0) {
			rpi_gpio_clearpin(GPIO_CH);
		}
		else {
			rpi_gpio_setpin(GPIO_CH);
		}
		gpiodrv_unmap();
	}
	jiffy_timer.data = jiffy_timer.expires;
	jiffy_timer.expires += JIFFY_DIFF;
	jiffy_timer.function = timer_handler;
	// re register
	add_timer(&jiffy_timer);
}

static int gpiodrv_init(void)
{
	int retval;

	/* starting message */
	printk(KERN_INFO "%s loading...\n", DEVNAME);

	/* To check mapping */
	retval = gpiodrv_map();
	if (retval != 0) {
		printk(KERN_ALERT "Can not use GPIO registers\n");
		return retval;
	}

	/* To init GPIO */
	gpiodrv_gpio_setup();

	/* Register device */
	retval = gpiodrv_register_dev();
	if (retval != 0) {
		printk(KERN_ALERT "gpiodrv register failed.\n");
		return retval;
	}

	// Success!!
	printk(KERN_INFO "gpiodrv register successed.\n");

	gpiodrv_unmap();
	/* register timer */
	register_jiffy_timer();
	return 0;
}

static void gpiodrv_exit(void)
{
	dev_t devno;
	int i;

	/* unregister timer */
	del_timer_sync(&jiffy_timer);
	/* To check mapping */
	if (gpiodrv_map() == 0) {
		/* end roll gpio */
		gpiodrv_gpio_endroll();
		gpiodrv_unmap();
	}

	/* unregister device */
	for ( i = 0; i < NUM_OF_DEVS; i++) {
		// delete node
		devno = MKDEV(_dev_major, _dev_minor + i);
		device_destroy(gp_class, devno);
		// unregister device
		cdev_del(&gp_cdev_array[i]);
	}
	kfree(gp_cdev_array);
	gp_cdev_array = NULL;
	/* destroy class */
	class_destroy(gp_class);
	/* purge allocated device number */
	devno = MKDEV(_dev_major, _dev_minor);
	unregister_chrdev_region(devno, NUM_OF_DEVS);
	// finish
	printk(KERN_INFO "gpiodrv removed.\n");
}

module_init(gpiodrv_init);
module_exit(gpiodrv_exit);
module_param( gi_gpio_level, int, S_IRUSR | S_IRGRP | S_IROTH );
