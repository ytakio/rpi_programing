#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/device.h>

#include "rpi2_gpio_reg.h"
#include "rpi_gpio.h"

MODULE_AUTHOR("Takio Yamaoka");
MODULE_LICENSE("MIT");

/* pointer to GPIO register */
static volatile uint32_t *gp_gpio_base = NULL;

static int rpi_gpio_function_set(int pin, uint32_t func)
{
	int index = RPI_GPFSEL0_INDEX + pin/10;
	uint32_t mask = ~(0x7 << ((pin%10) * 3));
	if (gp_gpio_base != NULL) {
		//printf("pin: %d, func: %d\n", pin, func);
		//printf("%#x <= %#x & %#x\n", index * sizeof(uint32_t), mask, ((func&0x7) << ((pin%10) * 3)) );
		gp_gpio_base[index] = (gp_gpio_base[index] & mask) | ((func&0x7) << ((pin%10) * 3));
		return 1;
	}
	else {
		printk(KERN_ALERT "GPIO register mapping not yet.\n");
		return 0;
	}
}

void rpi_gpio_init(uint32_t *p_base)
{
	gp_gpio_base = p_base;
}
EXPORT_SYMBOL(rpi_gpio_init);

void rpi_gpio_release(void)
{
	gp_gpio_base = NULL;
}
EXPORT_SYMBOL(rpi_gpio_release);

uint32_t rpi_gpio_getpin(int pin)
{
	uint32_t stat = -1;
	if (rpi_gpio_function_set(pin, RPI_GPF_INPUT) != 0) {
		stat = (gp_gpio_base[RPI_GPLEV0_INDEX] & (1 << pin)) != 0;
	}
	return stat;
}
EXPORT_SYMBOL(rpi_gpio_getpin);

void rpi_gpio_setpin(int pin)
{
	if (rpi_gpio_function_set(pin, RPI_GPF_OUTPUT) != 0) {
		printk(KERN_INFO "%s() called\n", __func__);
		gp_gpio_base[RPI_GPSET0_INDEX] = 1 << pin;
	}
}
EXPORT_SYMBOL(rpi_gpio_setpin);

void rpi_gpio_clearpin(int pin)
{
	if (rpi_gpio_function_set(pin, RPI_GPF_OUTPUT) != 0) {
		printk(KERN_INFO "%s() called\n", __func__);
		gp_gpio_base[RPI_GPCLR0_INDEX] = 1 << pin;
	}
}
EXPORT_SYMBOL(rpi_gpio_clearpin);
