#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "rpi_gpio.h"
#include "rpi_gpiolib.h"

#define USE_GPIO_PIN	7

int main(int argc, char const* argv[])
{
	int mode;
	int ret = 0;
	if (argc != 2) {
		fprintf(stderr, "usage: %s [value]\n", argv[0]);
		fprintf(stderr, "\t[value] 0: low output, 1: high output, -1: input\n");
		ret = -1;
	}
	else {
		// init
		if (rpi_gpio_init() == 0) {
			fprintf(stderr, "initial error\n");
			return -2;
		}
		// mode procedure
		mode = atol(argv[1]);
		if (mode < 0) {
			printf("input: %d\n", rpi_gpio_getpin(USE_GPIO_PIN) );
		}
		else if (mode == 0) {
			rpi_gpio_clearpin(USE_GPIO_PIN);
			printf("output: 0\n");
		}
		else if (mode == 1) {
			rpi_gpio_setpin(USE_GPIO_PIN);
			printf("output: 1\n");
		}
		else {
			fprintf(stderr, "not defined mode\n");
			ret = -3;
		}
		rpi_gpio_delete();
	}
	return ret;
}

