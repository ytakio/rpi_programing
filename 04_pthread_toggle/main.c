#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

#include <stdio.h>
#include <pthread.h>

static int f_stop = 0;

#define CASESIG(s) case s: printf(#s"\n"); break
static void signal_handler(int sig)
{
	switch (sig) {
		CASESIG(SIGINT);
		CASESIG(SIGTERM);
		CASESIG(SIGQUIT);
		CASESIG(SIGKILL);
	}
	f_stop++;
}

static void* inner_thread(void* arg)
{
	const char drvpath[] = "/dev/gpiodrv0";
	unsigned char i = 0;
	while (f_stop == 0) {
		int fd = open(drvpath, O_WRONLY | O_SYNC);
		if (0 < fd) {
			i++;
			write(fd, &i, sizeof(i));
			close(fd);
			usleep(500*1000);
		}
		else {
			fprintf(stderr, "can not open %s\n", drvpath);
			break;
		}
	}
}

int main(int argc, char const* argv[])
{
	pthread_t pth;
	// register signal
	f_stop = 0;
	signal( SIGINT, signal_handler );
	signal( SIGTERM, signal_handler );
	signal( SIGQUIT, signal_handler );
	signal( SIGKILL, signal_handler );
	// create thread
	pthread_create(&pth, NULL, inner_thread, (void*)0xdeadbeaf);
	printf("waiting...\n");
	pthread_join(pth, NULL);
	printf("finish.\n");
	return 0;
}

