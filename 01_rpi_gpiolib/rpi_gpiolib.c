#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdint.h>

#include "./rpi_gpio.h"
#include "./rpi_gpiolib.h"

static volatile uint32_t *gpio_base = NULL;

static int rpi_gpio_function_set(int pin, uint32_t func)
{
	int index = RPI_GPFSEL0_INDEX + pin/10;
	uint32_t mask = ~(0x7 << ((pin%10) * 3));
	if (gpio_base != NULL) {
		//printf("pin: %d, func: %d\n", pin, func);
		//printf("%#x <= %#x & %#x\n", index * sizeof(uint32_t), mask, ((func&0x7) << ((pin%10) * 3)) );
		gpio_base[index] = (gpio_base[index] & mask) | ((func&0x7) << ((pin%10) * 3));
		return 1;
	}
	else {
		fprintf(stderr, "mapping not yet\n");
		return 0;
	}
}

int rpi_gpio_init(void)
{
	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	void *gpio_mmap;
	
	if (fd < 0) {
		fprintf(stderr, "can not open /dev/mem\n");
		return 0;
	}

	gpio_mmap = mmap(	NULL,
						RPI_BLOCK_SIZE,
						PROT_READ | PROT_WRITE,
						MAP_SHARED,
						fd,
						RPI_GPIO_BASE);
	if (gpio_mmap == MAP_FAILED) {
		fprintf(stderr, "can not mmap GPIO BASE: %#X\n", RPI_GPIO_BASE);
		return 0;
	}

	close(fd);
	gpio_base = (volatile uint32_t*)gpio_mmap;
	return 1;
}

int rpi_gpio_delete(void)
{
	munmap( (void*)gpio_base, RPI_BLOCK_SIZE);
	gpio_base = NULL;
	return 1;
}

int rpi_gpio_pull_controll(int pin, uint32_t mode)
{
	gpio_base[RPI_GPPUD_INDEX] = mode & 0x3;
	gpio_base[RPI_GPPUDCLK0_INDEX] = 1 << pin;
	usleep(100);	// wait more than 150 cycles
	gpio_base[RPI_GPPUDCLK0_INDEX] = 0;
	gpio_base[RPI_GPPUD_INDEX] = 0;
	return 1;
}

void rpi_gpio_set32(uint32_t mask, uint32_t val)
{
	gpio_base[RPI_GPSET0_INDEX] = val & mask;
}

uint32_t rpi_gpio_get32(uint32_t mask)
{
	return gpio_base[RPI_GPLEV0_INDEX] & mask;
}

void rpi_gpio_clear32(uint32_t mask, uint32_t val)
{
	gpio_base[RPI_GPCLR0_INDEX] = val & mask;
}

void rpi_gpio_setpin(int pin)
{
	rpi_gpio_function_set(pin, RPI_GPF_OUTPUT);
	gpio_base[RPI_GPSET0_INDEX] = 1 << pin;
}

uint32_t rpi_gpio_getpin(int pin)
{
	rpi_gpio_function_set(pin, RPI_GPF_INPUT);
	return (gpio_base[RPI_GPLEV0_INDEX] & (1 << pin)) != 0;
}

void rpi_gpio_clearpin(int pin)
{
	rpi_gpio_function_set(pin, RPI_GPF_OUTPUT);
	gpio_base[RPI_GPCLR0_INDEX] = 1 << pin;
}

