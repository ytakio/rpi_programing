#ifndef _RPI_GPIO_H
#define _RPI_GPIO_H

#include <stdint.h>

//#define RPI_REG_BASE	0x20000000	// RPi 1
#define RPI_REG_BASE	0x3F000000	// Rpi 2
#define RPI_GPIO_BASE	(RPI_REG_BASE + 0x200000)	// + 0020_000
#define RPI_BLOCK_SIZE	(4 * 1024)

#define RPI_GPFSEL0_INDEX	0
#define RPI_GPFSEL1_INDEX	1
#define RPI_GPFSEL2_INDEX	2
#define RPI_GPFSEL3_INDEX	3

#define RPI_GPSET0_INDEX	7
#define RPI_GPCLR0_INDEX	10
#define RPI_GPLEV0_INDEX	13
#define RPI_GPPUD_INDEX		37
#define RPI_GPPUDCLK0_INDEX	38

#define RPI_GPFSEL0_OFFSET		( RPI_GPFSEL0_INDEX * sizeof(uint32_t) )
#define RPI_GPFSEL1_OFFSET		( RPI_GPFSEL1_INDEX * sizeof(uint32_t) )
#define RPI_GPFSEL2_OFFSET		( RPI_GPFSEL2_INDEX * sizeof(uint32_t) )
#define RPI_GPFSEL3_OFFSET		( RPI_GPFSEL3_INDEX * sizeof(uint32_t) )

#define RPI_GPSET0_OFFSET		( RPI_GPSET0_INDEX * sizeof(uint32_t) )
#define RPI_GPCLR0_OFFSET		( RPI_GPCLR0_INDEX * sizeof(uint32_t) )
#define RPI_GPLEV0_OFFSET		( RPI_GPLEV0_INDEX * sizeof(uint32_t) )
#define RPI_GPPUD_OFFSET		( RPI_GPPUD_INDEX * sizeof(uint32_t) )
#define RPI_GPPUDCLK0_OFFSET	( RPI_GPPUDCLK0_INDEX * sizeof(uint32_t) )

#define RPI_GPF_INPUT	0
#define RPI_GPF_OUTPUT	1
#define RPI_GPF_ALT0	0x04
#define RPI_GPF_ALT1	0x05
#define RPI_GPF_ALT2	0x06
#define RPI_GPF_ALT3	0x07
#define RPI_GPF_ALT4	0x03
#define RPI_GPF_ALT5	0x02

#define RPI_GPIO_PULLNONE	0x00
#define RPI_GPIO_PULLDOWN	0x01
#define RPI_GPIO_PULLUP		0x02

#define RPI_GPIO_P1MASK	(uint32_t)(	(1 << 2) |	(1 << 3) |	(1 << 4) | \
									(1 << 7) |	(1 << 8) |	(1 << 9) | \
									(1 << 10) |	(1 << 11) |	(1 << 14) | \
									(1 << 15) |	(1 << 17) |	(1 << 18) | \
									(1 << 22) |	(1 << 23) |	(1 << 24) | \
									(1 << 25) |	(1 << 27) \
		)

#define RPI_GPIO_P5MASK	(uint32_t)(	(1 << 28) |	(1 << 29) |	(1 << 30) |	(1 << 31))

#endif
