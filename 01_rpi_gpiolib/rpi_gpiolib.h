#ifndef _RPI_GPIOLIB_H
#define _RPI_GPIOLIB_H

int rpi_gpio_init(void);
int rpi_gpio_delete(void);
int rpi_gpio_pull_controll(int pin, uint32_t mode);
void rpi_gpio_set32(uint32_t mask, uint32_t val);
uint32_t rpi_gpio_get32(uint32_t mask);
void rpi_gpio_clear32(uint32_t mask, uint32_t val);
void rpi_gpio_setpin(int pin);
uint32_t rpi_gpio_getpin(int pin);
void rpi_gpio_clearpin(int pin);

#endif
