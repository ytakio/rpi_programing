#ifndef _RPI_GPIO_H
#define _RPI_GPIO_H

#include <linux/types.h>

void rpi_gpio_init(uint32_t *p_base);
void rpi_gpio_release(void);
uint32_t rpi_gpio_getpin(int pin);
void rpi_gpio_setpin(int pin);
void rpi_gpio_clearpin(int pin);

#endif
